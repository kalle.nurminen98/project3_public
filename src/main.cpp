#include <iostream>
#include <vector>
#include <string>
#include "read_file.h"
#include "functions.h"
#include "save_file.h"

std::vector<std::string> line; //To hold a single line of file content
std::vector<std::vector<std::string>> filecontent; // vector to hold all lines of file content

// Some variables to hold temporary data
int count;
std::string id;
int n;
int empty_slots;


int main(){


    // Load file content to filecontent vector
    filecontent = read_file(); 

    // Initialization of structs to hold statistics data
    struct machine m1;
    struct machine m2;
    struct machine m3;

    
    int fileSize = filecontent.size();

    for(int i = 0; i < fileSize; i++){
        //going through every line of filecontent
        line = filecontent[i];
        id = line[1]; // which machine the line is about
        count = onesCount(line); //get the count of ones in the line
        if(count == -1){ // error return from onesCount
            continue;
        }

        n = std::stoi(line[2]); // integer the the number of slots


        empty_slots = n - count;
        // If amount of zeroes is equal to previous "best"
        if(id == "M1" && empty_slots == m1.most){
            m1.mostCount++;
        }else if(id == "M2" && empty_slots == m2.most){
            m2.mostCount++;
        }else if(id == "M3" && empty_slots == m3.most){
            m3.mostCount++;
        }
        
        // Update statistics if machine on a line is full
        if(id == "M1" && isEmpty(line)){
            m1.empty++;
        }else if(id == "M1" && isFull(line)){
            m1.full++;
        }else if(id == "M2" && isEmpty(line)){
            m2.empty++;
        }else if(id == "M2" && isFull(line)){
            m2.full++;
        }else if(id == "M3" && isEmpty(line)){
            m3.empty++;
        }else if(id == "M3" && isFull(line)){
            m3.full++;
        }
        // Machine is neither empty or full
        // Will be checked if it is less empty than previous least empty
        else if(id == "M1" && empty_slots < m1.most){
            
            m1.most = empty_slots;
            m1.mostCount = 1;
            
        }else if(id == "M2" && empty_slots < m2.most){
            
            m2.most = empty_slots;
            m2.mostCount = 1;
            
        }else if(id == "M3" && empty_slots < m3.most){
            
            m3.most = empty_slots;
            m3.mostCount = 1;
            
        }

    }
    // Statistics are saved to a text file in this function
    fileSave(m1, m2, m3);
    // std::cout<<"M1: "<<m1.empty<<m1.full<<m1.most<<m1.mostCount<<std::endl;
    // std::cout<<"M2: "<<m2.empty<<m2.full<<m2.most<<m2.mostCount<<std::endl;
    // std::cout<<"M3: "<<m3.empty<<m3.full<<m3.most<<m3.mostCount<<std::endl;


    return 0;
}