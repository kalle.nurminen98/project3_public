## Assignment 3

This program reads FleetState.txt file, which contains comma separated data about machines and how many
slots are empty for each machine. The program then counts the amount of empty and full machines of each type, and outputs a Statistics.txt file which contains the statistics.

The repository has a pipeline, that compiles the program, runs unit tests, and runs the program. Running the program produces Statistics.txt as a pipeline artifact.

Kalle has made reading the file into usable vectors, and validating the vector content.

Juho has made statistics counting functions, output file saving, unit tests, gitlab and pipeline.

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Requirements

Requires  `gcc`, `make`, `CMake`,`gtest`.

## Usage

Running the program produces statistics file, where the results can be viewed.

## Error handling

Program can handle the following invalid inputs and will result in skipping a line:  
N is not a number  
Number of slots doesn't match N

M# can be whatever, only 1,2,3 are counted

Content of first entry of the line is not checked anywhere so it shouldn't result in a crash.

# Missing features and bugs and other assumptions
Only machines in existence "M1", "M2, "M3", others would be ignored

Each text line is a new machine, no checks for unique machine ids.

Slot being anything else than "1" should result the slot being counted as 0






# Create build directory and move into it
mkdir build && cd build

# Run Cmake to make makefile
cmake ../

# Compile the program
make

# Run tests
make tests

or

./tests

# Run the compiled binary
./main


## Maintainers

[Juho Kangas] (https://gitlab.com/jhkangas3)

[Kalle Nurminen] (https://gitlab.com/kalle.nurminen98)  